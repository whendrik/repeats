# Repeats

Code snippets I repeated, re-googled, copied...


## Partition sets for chunking loops

`toolz` is a nice python package to for instance create chunks with

```
from toolz.itertoolz import partition_all
```

## Convert timestring in pandas & parse dates

### Manually...

```
df["seconds_duration_as_td"] = df["seconds_duration"].apply(pd.to_timedelta).dt.total_seconds()
df["datetime"] = df["date"].apply(pd.to_datetime,  errors='coerce')
```

### During `read_csv()`...

```
dateparse = lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S')
df = pd.read_csv(infile, parse_dates=['datetime'], date_parser=dateparse)
```
-or-
```
df = pd.read_csv(infile, parse_dates=['datetime'], infer_datetime_format=True)
```

Only use `infer_datetime_format` if datetime format is consistent within whole column, as it infers from first few.

## Check if a range of days has missing days

```
first_day = min(water_levels.datetime)
last_day = max(water_levels.datetime)

date_difference = pd.date_range(start=first_day, end=last_day).difference( water_levels.datetime)

assert len(date_difference) == 0
```

## Pandas TimeSeries - Subpots years with `.dt.dayofyear` trick

```
%matplotlib inline
water_levels.pivot_table(index=water_levels.datetime.dt.dayofyear, columns=water_levels.datetime.dt.year, values="value").plot(sharex=None, subplots=True,figsize=(16,9))
```

## Create subplots by {year, month, week} or so

```
plt = pd_weather_concat.pivot_table(index=pd_weather_concat.datetime.dt.dayofyear, columns=pd_weather_concat.datetime.dt.year, values=["precipitationRate"]).plot(subplots=True,figsize=(16,9))
```

## Parse JSON files to CSV

Make use of Pandas! - Pandas eat dictionairies

```
pd_weatherobservarions = []

for json_file in json_weather_filenames:
    with open(json_file, 'r') as json_fo:  
        parsed_weather = json.load(json_fo)
        pd_weatherobservarions.append(pd.DataFrame(parsed_weather))
```
=======

## Flatten a (aggregated) DataFrame with Levels

(or reset index, but that can be hmmm)
```
flattened = pd.DataFrame(df.to_records())
# possibly...
flattened.columns = [a.replace("', '","_").strip("()'") for a in flattened.columns]
```

or reset index!

## Groupby with missing values for missing groups

https://stackoverflow.com/questions/42854801/including-missing-combinations-of-values-in-a-pandas-groupby-aggregation

## rsync over ssh
```
rsync -avzhe 'ssh -p 5432' root@some.where.com:/data/* .
```



---


# Misc Links

- carbon.now.sh
- https://pandas.pydata.org/pandas-docs/version/0.23.4/generated/pandas.melt.html
